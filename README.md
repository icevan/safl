# SafL Simple af Launcher

## Getting started

SafL is a simple app/game launcher made in python3. Just put image(s) (png/jpg/jpeg/gif) inside /img and a bash script with the same name as the image but without extension (example included) and running permissions (chmod +x filename), it will be shown in the launcher GUI and if the image is double clicked, it will launch the script with the same name.

##Dependencies
Sudo apt instal python3-qt5

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
GNU.
